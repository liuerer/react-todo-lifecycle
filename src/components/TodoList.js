import React, {Component} from 'react';
import './todolist.less';

let count = 0;
class TodoList extends Component {

  constructor(props){
    console.log('constructor');
    super(props);
    this.state={
      index: 0
    }
  }

  handleOnClick(){
    this.setState({
      index: Number.parseInt(this.state.index) + 1
    });
  }


  render() {
    console.log('render');
    let listItem = [];
    for(let i = 1; i<=this.state.index; ++i){
      listItem.push(<li key={i} className='listTittle'>List Tittle {i}</li>);
    }
    return (
      <div id='todoList'>
        <button className='addButton' onClick={()=>this.handleOnClick()}>Add</button>
        <ul>
          {listItem}
        </ul>
      </div>
    );
  }

  componentDidMount() {
    console.log('componentDidMount');
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }
}

export default TodoList;

