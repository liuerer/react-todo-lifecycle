import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component{

	render() {
		return (
			<div className='App'>
				<section className='menu'>
					<button id='button1' onClick={()=>this.handleOnClickShowOrHide()}>Hide</button>
					<button id='button2' onClick={()=>this.handleOnClickRefresh()}>Refresh</button>
				</section>
				<TodoList id='todoList'/>
			</div>
		);
	}

	handleOnClickShowOrHide() {
		const todoListElement = document.getElementById('todoList');
		const showButtonElement = document.getElementById('button1');
		if(showButtonElement.innerText === 'Hide'){
			todoListElement.hidden = true;
			showButtonElement.innerText = 'Show';
		} else {
			todoListElement.removeAttribute('Hidden');
			showButtonElement.innerText = 'Hide';
		}
	}

	handleOnClickRefresh() {
		location.reload();
	}


}

export default App;
